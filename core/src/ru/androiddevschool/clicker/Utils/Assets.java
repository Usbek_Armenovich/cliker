package ru.androiddevschool.clicker.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;

/**
 * Created by 02k1102 on 01.04.2017.
 */
public class Assets {
    private static Assets ourInstance = new Assets();

    public static Assets get() {
        return ourInstance;
    }

    private Assets() {
        initImages();
        initStyles();
        initAnimations();
        initFonts();
        initLabelStyles();
    }

    private void initLabelStyles() {
        labelStyles = new HashMap<String, Label.LabelStyle>();
        labelStyles.put("simple", new Label.LabelStyle(fonts.get("simple"), Color.GREEN));
    }

    private void initFonts() {
        fonts = new HashMap<String, BitmapFont>();
        fonts.put("simple", new BitmapFont());
    }

    private void initAnimations() {
        animations = new HashMap<String, Animation<TextureRegionDrawable>>();
        Array<TextureRegionDrawable> frames = new Array<TextureRegionDrawable>();
        for (int i = 1; i <= 22; i++) frames.add(images.get(String.format("player_%d", i)));
        animations.put("dig", new Animation<TextureRegionDrawable>(0.03f, frames, Animation.PlayMode.LOOP));
    }

    private void initStyles() {
        buttonStyles = new HashMap<String, Button.ButtonStyle>();
        buttonStyles.put("play btn",
                new Button.ButtonStyle(
                        images.get("play"),
                        null,
                        null

                ));
        buttonStyles.put("back btn",
                new Button.ButtonStyle(
                        images.get("red_cross"),
                        null,
                        null

                ));
        buttonStyles.put("settings btn",
                new Button.ButtonStyle(
                        images.get("settingsbtn"),
                        null,
                        null
                ));
        buttonStyles.put("help btn",
                new Button.ButtonStyle(
                        images.get("helpbtn"),
                        null,
                        null
                ));
        buttonStyles.put("shop btn",
                new Button.ButtonStyle(
                        images.get("white_cross"),
                        null,
                        null
                ));
    }

    private void initImages() {
        images = new HashMap<String, TextureRegionDrawable>();
        addImagesFolder("/play");
        addImagesFolder("/UI");
        for (String name : images.keySet()) System.out.println(name);
    }

    public void addImagesFolder(String folderName) {
        FileHandle file = Gdx.files.local(folderName);
        for (FileHandle f : file.list()) {
            if (f.name().endsWith("png"))
                images.put(f.nameWithoutExtension(), getDrawable(f));
        }
    }

    public TextureRegionDrawable getDrawable(FileHandle file) {
        return new TextureRegionDrawable(new TextureRegion(new Texture(file)));
    }

    public HashMap<String, TextureRegionDrawable> images;
    public HashMap<String, Button.ButtonStyle> buttonStyles;
    public HashMap<String, Animation<TextureRegionDrawable>> animations;
    public HashMap<String, Label.LabelStyle> labelStyles;
    public HashMap<String, BitmapFont> fonts;
}