package ru.androiddevschool.clicker.Utils;

import com.badlogic.gdx.Gdx;

/**
 * Created by 02k1102 on 08.04.2017.
 */
public class Values {

    public static final float WORLD_WIDTH = 1080;
    public static final float WORLD_HEIGHT = 1920;
    public static final float ppuX = Gdx.graphics.getWidth()/WORLD_WIDTH;
    public static final float ppuY = Gdx.graphics.getHeight()/WORLD_HEIGHT;
    public static final float playerVelocity = 100f;
}
