package ru.androiddevschool.clicker;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;

import ru.androiddevschool.clicker.Screens.Help;
import ru.androiddevschool.clicker.Screens.Menu;
import ru.androiddevschool.clicker.Screens.Play;
import ru.androiddevschool.clicker.Screens.Settings;
import ru.androiddevschool.clicker.Screens.Shop;

public class SashaClicker extends Game {
	private static SashaClicker instance = new SashaClicker();
	private SashaClicker(){}
	public static SashaClicker get(){return instance;}
	private SpriteBatch batch;
	private HashMap<String, Screen> screens;
	public void setScreen(String name) {
		if (screens.containsKey(name))
			setScreen(screens.get(name));
	}

	@Override
	public void create() {
		saveFile = Gdx.app.getPreferences("save");
		if (!saveFile.contains("coins")) saveFile.putInteger("coins", 0);
		if (!saveFile.contains("maxEnergy")) saveFile.putInteger("maxEnergy", 500);
		if (!saveFile.contains("energy")) saveFile.putInteger("energy", 500);
		if (!saveFile.contains("speed")) saveFile.putInteger("speed", 100);

		batch = new SpriteBatch();
		screens = new HashMap<String, Screen>();
		Gdx.gl.glClearColor(0, 1, 1, 1);
		screens.put("Menu", new Menu(batch));
		screens.put("Play", new Play(batch));
		screens.put("Settings", new Shop(batch));
		screens.put("Help", new Help(batch));
		screens.put("Shop", new Settings(batch));
				setScreen("Menu");
	}



	public void save(String type, int value) {
		saveFile.putInteger(type, value);
	}

	public void addValue(String type, int dvalue) { saveFile.putInteger(type, saveFile.getInteger(type)+dvalue);}

	public int load(String type) {
		return saveFile.getInteger(type);
	}


	private Preferences saveFile;

}
