package ru.androiddevschool.clicker.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.clicker.SashaClicker;
import ru.androiddevschool.clicker.Utils.Assets;

import static com.badlogic.gdx.controllers.ControlType.button;

/**
 * Created by User on 12.05.2017.
 */

public class Shop extends StdScreen {

    public Shop(SpriteBatch batch) {
        super(batch);
    }
    @Override
    protected Stage initUi(Stage ui) {
        Table table = new Table();
        table.setFillParent(true);
        Label label;
        Button button;

        label = new Label("Max energy (10) ", Assets.get().labelStyles.get("simple"));
        button = new Button(Assets.get().buttonStyles.get("shop btn"));
        button.addListener (new ClickListener(){
            public void clicked(InputEvent event, float x, float y){
                SashaClicker.get().addValue("maxEnergy", 10);
                SashaClicker.get().addValue("coins", -10);
            }
        });
        table.add(label).pad(30);
        table.add(button).pad(30).row();


        label = new Label("Speed (30) ", Assets.get().labelStyles.get("simple"));
        button = new Button(Assets.get().buttonStyles.get("shop btn"));
        button.addListener (new ClickListener(){
            public void clicked(InputEvent event, float x, float y){
                SashaClicker.get().addValue("speed", 5);
                SashaClicker.get().addValue("coins", -30);
            }
        });
        table.add(label).pad(30);
        table.add(button).pad(30).row();


        ui.addActor(table);

        return ui;
    }

    @Override
    protected Stage initWorld(Stage stage) {
        return stage;
    }

    @Override
    protected Stage initBg(Stage bg) {
        Image background = new Image(Assets.get().images.get("shopbg"));
        bg.addActor(background);

        return bg;
    }

    @Override
    protected void postAct() {

    }
}
