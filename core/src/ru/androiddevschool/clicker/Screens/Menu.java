package ru.androiddevschool.clicker.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ru.androiddevschool.clicker.Controller.ScreenTraveler;
import ru.androiddevschool.clicker.Utils.Assets;

/**
 * Created by 02k1102 on 01.04.2017.
 */

public class Menu extends StdScreen {
    public Menu(SpriteBatch batch) {
        super(batch);

    }

    protected Stage initBg(Stage bg){
        Image background = new Image(Assets.get().images.get("daybg"));
        bg.addActor(background);

        return bg;

    }
    protected Stage initWorld(Stage stage){

        return stage;
    }
    protected Stage initUi(Stage ui){

        Button button;
        Table table = new Table();
        table.setFillParent(true);

        button = new Button(Assets.get().buttonStyles.get("settings btn"));
        button.addListener(new ScreenTraveler("Settings"));
        table.add(button).expand().bottom().left();

        button = new Button(Assets.get().buttonStyles.get("play btn"));
        button.addListener(new ScreenTraveler("Play"));
        table.add(button).padTop(100).padLeft(400);

        button = new Button(Assets.get().buttonStyles.get("help btn"));
        button.addListener(new ScreenTraveler("Help"));
        table.add(button).right().bottom();

        ui.addActor(table);
        return ui;
    }
    protected void postAct(){

    }
}