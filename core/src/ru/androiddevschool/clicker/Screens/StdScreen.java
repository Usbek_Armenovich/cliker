package ru.androiddevschool.clicker.Screens;

/**
 * Created by 02k1102 on 01.04.2017.
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import ru.androiddevschool.clicker.Utils.Values;

abstract class StdScreen implements Screen { //класс меню, который является экраном
    protected OrthographicCamera camera; //камера, которая будет смотреть на сцену
    protected Stage stage; //сама сцена
    protected OrthographicCamera uicamera; //камера, которая будет смотреть на геймплей
    protected Stage ui; //интерфейс - наши кнопки
    protected OrthographicCamera bgcamera; //камера, которая будет смотреть на геймплей
    protected Stage bg; //интерфейс - наши кнопки
    protected InputMultiplexer multiplexer;

    public StdScreen(SpriteBatch batch) {
        bgcamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        bgcamera.setToOrtho(false);
        bg = new Stage(new StretchViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, bgcamera), batch);   camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()); //создаем камеру и задаем размеры
        camera.setToOrtho(false); //направляем ось Oy вниз
        stage = new Stage(new StretchViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, camera), batch); //создаем сцену с камерой и отрисовщиком

        uicamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        uicamera.setToOrtho(false);
        ui = new Stage(new StretchViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, uicamera), batch);

        bg = initBg(bg);
        stage = initWorld(stage);
        ui = initUi(ui);
    }

    abstract protected Stage initUi(Stage ui);

    abstract protected Stage initWorld(Stage stage);

    abstract protected Stage initBg(Stage bg);

    @Override
    public void show() {
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(ui);
        multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer); //все нажатия на libGDX Будет обрабатывать сцена этого экрана
    }

    protected void act(float delta) {
        ui.act(delta);
        stage.act(delta); //сдвинуть всех актеров на сцене

    }

    abstract protected void postAct();

    protected void draw() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT); //закрашиваем экран белым цветом
        bg.draw();
        stage.draw(); //отрисовать сцену
        ui.draw();
    }


    @Override
    public void render(float delta) {
        act(delta);
        postAct();
        draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null); //сцена прекращает обработку нажатия
    }

    @Override
    public void dispose() {
        stage.dispose(); // при умирании экрана убить сцену
    }
}
