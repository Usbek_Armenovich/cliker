package ru.androiddevschool.clicker.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ru.androiddevschool.clicker.Controller.ScreenTraveler;
import ru.androiddevschool.clicker.Utils.Assets;

/**
 * Created by User on 07.04.2017.
 */

public class Settings extends StdScreen {
    public Settings(SpriteBatch batch) {
        super(batch);
    }

    @Override
    protected Stage initUi(Stage ui) {
        Button button;
        Table table = new Table();
        table.setFillParent(true);

        button = new Button(Assets.get().buttonStyles.get("back btn"));
        button.addListener(new ScreenTraveler("Menu"));
        table.add(button).expandX().top().left().padRight(30).padTop(30).row();
        table.add().expand(true,true).row();
        ui.addActor(table);
        return ui;
    }

    @Override
    protected Stage initWorld(Stage stage) {

        return stage;
    }

    @Override
    protected Stage initBg(Stage bg) {
        Image background = new Image(Assets.get().images.get("daybg"));
        bg.addActor(background);

        return bg;
    }

    @Override
    protected void postAct() {

    }
}