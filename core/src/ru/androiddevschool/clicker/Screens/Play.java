package ru.androiddevschool.clicker.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ru.androiddevschool.clicker.Controller.ScreenTraveler;
import ru.androiddevschool.clicker.Model.World;
import ru.androiddevschool.clicker.Utils.Assets;

/**
 * Created by User on 07.04.2017.
 */

public class Play extends StdScreen {
    private World world;
    private Label coins;
    private Label energy;
    public Play(SpriteBatch batch) {
        super(batch);
    }

    protected Stage initBg(Stage bg){
        Image background = new Image(Assets.get().images.get("ground"));
        bg.addActor(background);

        return bg;
    }
    protected Stage initWorld(Stage stage){
        world = new World(stage.getViewport(), stage.getBatch());

        return world;
    }
    protected Stage initUi(Stage ui){
        Button button;
        Table table = new Table();
        table.setFillParent(true);

        button = new Button(Assets.get().buttonStyles.get("back btn"));
        button.addListener(new ScreenTraveler("Menu"));
        table.add(button).expandX().top().left().padRight(30).padTop(30).row();

        energy = new Label("", Assets.get().labelStyles.get("simple"));
        coins = new Label("", Assets.get().labelStyles.get("simple"));
        table.add(energy).padBottom(30);
        table.add(coins).padBottom(30).row();
        table.add().expand(true,true).row();
        ui.addActor(table);
        return ui;
    }
    protected void postAct(){
        world.stickToPlayer();
    }

    public void show(){
        super.show();
        world.updatePlayer();
    }
    public void hide(){
        super.hide();
        world.savePlayer();
    }
}