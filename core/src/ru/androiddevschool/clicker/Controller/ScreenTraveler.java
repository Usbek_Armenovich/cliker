package ru.androiddevschool.clicker.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import ru.androiddevschool.clicker.SashaClicker;

/**
 * Created by ga_nesterchuk on 04.03.2017.
 */
public class ScreenTraveler extends ClickListener {
    private String name;
    public ScreenTraveler(String name){
        this.name = name;
    }
    public void clicked(InputEvent event, float x, float y) {//куда кнопка будет переводить
        SashaClicker.get().setScreen(name);
    }
}
