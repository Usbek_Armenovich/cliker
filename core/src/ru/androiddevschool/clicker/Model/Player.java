package ru.androiddevschool.clicker.Model;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import ru.androiddevschool.clicker.Utils.Values;

/**
 * Created by 02k1102 on 06.05.2017.
 */
public class Player extends Image{
    private Animation<TextureRegionDrawable> animation;
    private float time;
    private float count;
    public int speed;
    public int coins;
    public int energy;

    public Player(Animation<TextureRegionDrawable> animation) {
        super(animation.getKeyFrame(0));
        this.animation = animation;
        this.time = 0;
        this.count = 0;
    }

    public void act(float delta){
        this.time+=delta;
        if (this.time > animation.getAnimationDuration()){
            this.time -= animation.getAnimationDuration();
            if (count > 0) count--;
        }
        if (count > 0) {
            setDrawable(animation.getKeyFrame(time));
            moveBy(0, -speed * delta);
        }
    }

    public void tap(){
        if (count == 0) time = 0;
        count++;
    }

}
