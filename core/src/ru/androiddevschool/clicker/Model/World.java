package ru.androiddevschool.clicker.Model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.Viewport;

import ru.androiddevschool.clicker.SashaClicker;
import ru.androiddevschool.clicker.Utils.Assets;
import ru.androiddevschool.clicker.Utils.Values;

/**
 * Created by 02k1102 on 06.05.2017.
 */
public class World extends Stage {
    Player player;
    Image daybg;

    public World(Viewport viewport, Batch batch) {
        super(viewport, batch);
        daybg = new Image(Assets.get().images.get("daybg"));
        addActor(daybg);
        player = new Player(Assets.get().animations.get("dig"));
        player.setPosition(Values.WORLD_WIDTH / 2 , daybg.getHeight()-1100, Align.bottom);
        addActor(player);
    }

    public void stickToPlayer() {
        getCamera().position.x = player.getX()+player.getWidth()/2;
        getCamera().position.y = player.getY() + 300;
    }

    public boolean touchDown (int screenX, int screenY, int pointer, int button) {
        player.tap();
        return false;
    }

    public void updatePlayer(){
        player.coins = SashaClicker.get().load("coins");
        player.speed = SashaClicker.get().load("speed");
        player.energy = SashaClicker.get().load("energy");
    }

    public void savePlayer() {
        SashaClicker.get().save("coins", player.coins);
        SashaClicker.get().save("energy", player.energy);
    }
    public Player getPlayer(){
        return player;
    }
}
